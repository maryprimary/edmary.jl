#=
测试态
=#


include("../src/edmary.jl")

using ..edmary


function sta1()
    csn = count_state_num(4, 16)
    println(csn)
    arrs, arrsinv = get_state_array(2, 4)
    println(arrs, arrsinv)
    println(arrs[4])
    arr = arrs[4]
    uparr, dnarr = arr[1:4], arr[5:end]
    println(uparr)
    arr1 = apply_creation(uparr, 2)
    println(arr1)
    println(dnarr)
    arr1 = apply_creation(dnarr, 2)
    println(arr1)
    #
    println(have_bond(uparr, dnarr, 1, 3))
end



sta1()

