#=
测试哈密顿量构造
=#


include("../src/edmary.jl")

using ..edmary

using LinearAlgebra
using Plots


function idx2uxuy(L, idx)
    ux = mod(idx, 4)
    if ux == 0
        ux = L
    end
    #
    uy = (idx - ux)/L + 1
    return ux, uy
end



function ckb1()
    bonds = bonds_ckb(4, -1.0, -1.0)
    htmp = zeros(16, 16)
    for bnd in bonds
        htmp[bnd[1], bnd[2]] += bnd[3]
        htmp[bnd[2], bnd[1]] += bnd[3]
    end
    println(eigvals(htmp))
    #
    plt = plot()
    for i in Base.OneTo(16)
        ux, uy = idx2uxuy(4, i)
        scatter!(plt, [ux], [uy], label="", color=:black)
        annotate!(plt, ux, uy, string(i))
    end
    #
    #for bnd in bonds
    #    ux1, uy1 = idx2uxuy(4, bnd[1])
    #    ux2, uy2 = idx2uxuy(4, bnd[2])
    #    plot!(plt, [ux1, ux2], [uy1, uy2], label="", color=:red)
    #end
    ppbonds = ppbonds_ckb(4,
    Dict("+x"=>1, "+y"=>-1, "-x"=>1, "-y"=>-1, "+x+y"=>im, "-x-y"=>im),
    Dict("+x"=>1, "+y"=>-1, "-x"=>1, "-y"=>-1, "+x-y"=>-im, "-x+y"=>-im),
    )
    clrdict = Dict(1=>:red, -1=>:blue, im=>:green, -im=>:pink)
    for bnd in ppbonds
        ux1, uy1 = idx2uxuy(4, bnd[1])
        ux2, uy2 = idx2uxuy(4, bnd[3])
        if bnd[2] == :up
            plot!(plt, [ux1, ux2], [uy1, uy2], label="", color=clrdict[bnd[5]])
        end
    end
    savefig(plt, "test.png")
    #
    corr = ppbuild_from_order_param(ppbonds, [1, 5])
    #println(corr)
    println(size(corr))
end



function ckb2()
    bonds = bonds_ckb(4, -1.0, -0.0)
    htmp = zeros(16, 16)
    for bnd in bonds
        htmp[bnd[1], bnd[2]] += bnd[3]
        htmp[bnd[2], bnd[1]] += bnd[3]
    end
    println(eigvals(htmp))
    evec = eigvecs(htmp)
    evec = evec[:, 1:3]
    gf = evec * adjoint(evec)
    println(gf)
    println(gf[2, 1], " ", gf[2, 13])
    println(gf[1, 1], " ", gf[2, 2])
end



ckb2()

