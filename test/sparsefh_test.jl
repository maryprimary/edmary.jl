#=
测试哈密顿量构造
=#


include("../src/edmary.jl")

using ..edmary

using SparseArrays
using LinearAlgebra
using Arpack: eigs


function fullham1()
    sta, invsta = get_state_array(2, 3)
    println(sta)
    bonds = [(1, 2, -0.2), (1, 3, -0.8)]
    fh = full_hamiltonian(2, 3, sta, invsta, bonds, 2.0)
    println(fh)
    #
    nurand = rand(length(sta))
    nutmp = zeros(length(sta))
    apply_hmlt!(nutmp, nurand, 3, sta, invsta, bonds, 2.0)
    println(nutmp)
    println(fh*nurand)
    #
    eval, evec = eigen(fh)
    println(eval)
end


function fullham2()
    sta, invsta = get_state_array(2, 3)
    println(sta)
    bonds = [(1, 2, -0.2), (1, 3, -0.8)]
    fh = full_hmltCSC(2, 3, sta, invsta, bonds, 2.0)
    println(Matrix(fh))
    #
    nurand = rand(length(sta))
    nutmp = zeros(length(sta))
    apply_hmlt!(nutmp, nurand, 3, sta, invsta, bonds, 2.0)
    println(nutmp)
    println(fh*nurand)
    #
    v0r = rand(length(sta))
    v0r = v0r / norm(v0r)
    eval, evec = eigs(fh; nev=5, which=:SR, tol=0.0, maxiter=100, sigma=nothing, ritzvec=true, v0=v0r)
    println(eval)
end


fullham1()
fullham2()

