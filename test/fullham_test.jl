#=
测试哈密顿量构造
=#


include("../src/edmary.jl")

using ..edmary


function fullham1()
    sta, invsta = get_state_array(2, 3)
    println(sta)
    bonds = [(1, 2, -0.2), (1, 3, -0.8)]
    fh = full_hamiltonian(2, 3, sta, invsta, bonds, 2.0)
    println(fh)
    #
    nurand = rand(length(sta))
    nutmp = zeros(length(sta))
    apply_hmlt!(nutmp, nurand, 3, sta, invsta, bonds, 2.0)
    println(nutmp)
    println(fh*nurand)
end


fullham1()
